const Error = { template: '<div><p>Something go wrong :( </p><router-link to="/">Go back</router-link></div>' }

export default [
    {
        path: '/',
        name: 'index',
        component: () => import('../views/CinemaHall'),
    },{
        path: '/thank-you',
        name: 'thank-you',
        component: () => import('../views/ThankYou'),
    }, {
        path: "/:catchAll(.*)",
        name: 'error',
        component: () => Error,
    },
]