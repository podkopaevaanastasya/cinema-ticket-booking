import { createApp } from 'vue'

import App from './App.vue'
import router from './router'
import store from './store'

import PrimeVue from 'primevue/config'
import Tooltip from 'primevue/tooltip'

import 'primeflex/primeflex.css'
import "primevue/resources/primevue.min.css"
import "primevue/resources/themes/saga-blue/theme.css"

const app = createApp(App)
app.use(router)
app.use(store)
app.use(PrimeVue)
app.directive('tooltip', Tooltip);
app.mount('#app')
