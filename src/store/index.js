import { createStore } from 'vuex'
import {booking} from "@/store/booking";

export default createStore({
    modules: {
        booking
    }
})