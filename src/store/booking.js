export const booking = {
    state: () =>({
        seats: [],
        bookedSeats: [],
    }),
    getters: {
        getSeats(state) {
            return state.seats
        },
        getBookedSeats(state) {
            return state.bookedSeats
        },
    },
    mutations: {
        ADD_SEATS(state, seats) {
            state.seats = seats
        },
        EDIT_TICKET_BOOKING_STATE(state, id) {
            const ticket = state.seats.find(el => el.id === id)
            if(ticket) {
                state.seats = [
                    ...state.seats.filter(el => el.id !== id),
                    { ...ticket, isBooked: !ticket.isBooked },
                ]
            }
            const index = state.bookedSeats.find(el => el === id)
            if(index) state.bookedSeats = state.bookedSeats.filter(el => el !== id)
            else state.bookedSeats.push(id)
        },
        BOOK_TICKETS(state) {
            let newArr = []
            state.seats.forEach(el => {
                if(state.bookedSeats.includes(el.id)) newArr.push({...el, isBooked: false, isOccupied: true})
                else newArr.push({...el})
            })
            state.seats = newArr
            state.bookedSeats = []
        }
    }
}