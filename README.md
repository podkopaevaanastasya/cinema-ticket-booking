### Welcome to my GitLab!  
Let me explain this project. It's a simple **cinema ticket booking app** where you can
look at cinema hall schema and choose seats to book. You can select several seats and delete seats
from selection easily. Chosen seats have yellow background, and occupied seats have gray background.
You can't choose occupied seats. Other users can't choose booked seats by you. After selecting seats
you should enter your email for full booking.  
I hope you will enjoy!

### Project setup
I use this command to install all project dependencies.
```
yarn install
```
I use this command to compile app locally.
```
yarn serve
```
